# FUNCTIONALITY FOR LOADING CORPORA
import csv
import nltk
import os
from settings import gnad_path, nela_path, sentiment140_path


def print_corpus_numbers(corpus):
    print("Corpus has:")
    print("- %d documents"%len(corpus))
    print("- %d words"%sum(map(len,corpus)))
    

def print_corpora_numbers(corpora):
    """Show numbers of documents and topics for several corpora.
    
    Corpora are assumed to be given as dict, where the keys are identifiers."""
    print("Obtained %d corpora"%len(corpora))
    for topic in corpora:
        print("\nCorpus '%s'"%topic)
        print_corpus_numbers(corpora[topic])


def load_corpora_from_csv(path, corpus_column_pos=0, content_column_pos=1, delimiter=",", encoding='utf8',
        corpus_name_mapping=None, sentences_as_docs=False, casual_tokenizer=False):
    corpora = {} # corpus names as keys

    for i,row in enumerate(csv.reader(open(path, 'r', encoding=encoding), delimiter=delimiter)):
        # identify the corpus name
        corpus = row[corpus_column_pos]
        if corpus_name_mapping is not None:
            corpus = corpus_name_mapping[corpus]

        # read the content
        content = row[content_column_pos]

        if not sentences_as_docs:
            content = nltk.casual_tokenize(content) if casual_tokenizer else nltk.tokenize.word_tokenize(content)
        else:
            content = [nltk.casual_tokenize(sent) if casual_tokenizer else nltk.tokenize.word_tokenize(sent)
                    for sent in nltk.sent_tokenize(content)]
        
        if corpus not in corpora:
            corpora[corpus] = []

        if not sentences_as_docs:
            corpora[corpus].append(content)
        else:
            corpora[corpus].extend(content)

    return corpora


def load_gnad(path=gnad_path):
    return load_corpora_from_csv(os.path.join(path,'articles.csv'), delimiter=";")


def load_sentiment140(path=sentiment140_path):
    return load_corpora_from_csv(os.path.join(path,'trainingandtestdata/training.1600000.processed.noemoticon.csv'),
                                content_column_pos=-1, casual_tokenizer=True, encoding='latin1',
                                corpus_name_mapping={'0': 'negative', '2': 'neutral', '4': 'positive'})

def load_nela_gt_2018(path=nela_path, sources=None, sentences_as_docs=False, tokenize=True):
    """Load the NELA GT 2018 news dataset.

    Note: The dataset is quite large, we might only want to read data from a specified list of sources."""
    corpora = {}

    for root, dirs, files in os.walk(path, topdown=False):
        for name in files:
            source = root.split('/')[-1]
            
            if sources is not None and source not in sources:
                break
                
            text = [line for line in open(os.path.join(root, name), 'r')]

            if source not in corpora:
                corpora[source] = []

            if sentences_as_docs:
                # version with sentences as documents
                corpora[source].extend([nltk.tokenize.word_tokenize(sent) if tokenize else sent
                                        for sent in nltk.sent_tokenize(''.join(text))])
            else:
                # articles as documents
                corpora[source].append(nltk.tokenize.word_tokenize(''.join(text)) if tokenize else ''.join(text))
            
    return corpora


