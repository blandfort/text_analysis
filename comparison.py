import numpy as np
from collections import Counter
from scipy.spatial.distance import pdist,squareform
from nltk.corpus import stopwords
import gensim


def build_vocab(corpus, min_occurrence=10, to_lower=True, whitelist=None):
    """Build a vocabulary from a single corpus, ordered by frequency (most common first)."""
    word_counts = Counter([word.lower() if to_lower else word for sent in corpus for word in sent
                          if whitelist is None or word in whitelist])
    
    return [w for w,c in word_counts.most_common() if c>=min_occurrence]

def build_common_vocab(corpora, max_size=5000, remove_stopwords=False):
    """Create a vocabulary of common words for several corpora.
    
    Note that the common vocabulary is not ordered by frequency anymore.
    
    :param corpora: Corpora given as dict with corpus names as keys.
    :param max_size: For each corpus the vocabulary will be cut after this number of words (starting from most common.)
    :param remove_stopwords: If True, (English) stopwords are removed."""
    vocabs = {corpus: build_vocab(corpora[corpus]) for corpus in corpora}
    if remove_stopwords:
        sws = set(stopwords.words('english'))
        temp_vocabs = {corpus: [token for token in vocabs[corpus] if token not in sws] for corpus in vocabs}
        vocabs = temp_vocabs
    vocabs = {corpus: set(vocabs[corpus][:max_size]) for corpus in vocabs}
    return list(set.intersection(*vocabs.values()))


def create_co_occurrence_matrix(corpus, content_extraction_fct=lambda x: list(set(x)), context_extraction_fct=lambda x: x):
    """Create a co-occurrence matrix from a given corpus.
    
    Arguments:
    - corpus: Corpus as list of documents (each document counts as a context)
    - content_extraction_fct: Function that takes a document and returns a list of contents considered for co-occurrences
    - context_extraction_fct: Function that takes a document and returns a list of contexts considered for co-occurrences"""
    co_occs = dict()
    
    for doc in corpus:
        
        context_concepts = Counter(context_extraction_fct(doc))
        
        for input_concept in content_extraction_fct(doc):
            if input_concept not in co_occs:
                co_occs[input_concept] = Counter()
            
            for context_concept,count in context_concepts.items():
                if context_concept!=input_concept:
                    co_occs[input_concept][context_concept] += count
                
    # convert the dictionary to a matrix
    input_vocab = sorted(list(co_occs.keys()))
    context_vocab = sorted(list(set([context for input_context in co_occs.values() for context in input_context.keys()])))
    co_occ_matrix = np.zeros((len(input_vocab), len(context_vocab)))
    
    for input_concept in co_occs:
        input_ix = input_vocab.index(input_concept)
        for context_concept,count in co_occs[input_concept].items():
            co_occ_matrix[input_ix,context_vocab.index(context_concept)] += count
    
    return co_occ_matrix, input_vocab, context_vocab


def create_co_occurrence_matrices(corpora, content_extraction_fct=lambda x: list(set(x)), context_extraction_fct=lambda x: x):
    com = {corpus: create_co_occurrence_matrix(corpora[corpus], content_extraction_fct, context_extraction_fct) for corpus in corpora}
    #matrix1, invocab1, covocab1 = list(com.values())[0]

    combined_invoc = list(set.union(*[set(com[corpus][1]) for corpus in com]))
    combined_covoc = list(set.union(*[set(com[corpus][2]) for corpus in com]))

    # for efficiency we once compute a mapping from new vocabulary words to indices
    invoc_mappings = {}
    covoc_mappings = {}
    for corpus in com:
        invoc = com[corpus][1]
        invoc_mappings[corpus] = {word: invoc.index(word) if word in invoc else -1 for word in combined_invoc}
        
        covoc = com[corpus][2]
        covoc_mappings[corpus] = {word: covoc.index(word) if word in covoc else -1 for word in combined_covoc}

    # updating the matrices according to combined vocabularies
    matrices = {corpus: np.zeros((len(combined_invoc), len(combined_covoc))) for corpus in com}

    for ix,in_concept in enumerate(combined_invoc):
        for jx,co_concept in enumerate(combined_covoc):
            for corpus in com:
                if invoc_mappings[corpus][in_concept]>=0 and covoc_mappings[corpus][co_concept]>=0:
                    matrices[corpus][ix,jx] = com[corpus][0][invoc_mappings[corpus][in_concept],covoc_mappings[corpus][co_concept]]
                    
    return matrices, combined_invoc, combined_covoc


def show_co_occurrence_differences(matrices, in_vocab, co_vocab, min_count=20, num_concepts=4, num_similar=5):
    """Make a list of co-occurrence differences across corpora

    Arguments:
    - min_count : only consider concepts with at least min_count number of context occurrences in each context
    """
    co_diffs = np.zeros(len(in_vocab))
    for ix in range(len(in_vocab)):

        if all(np.sum(abs(mat[ix]))>=min_count for mat in matrices.values()):
            # In case of two corpora, we just want to compute the cosine distance between the two co-occurrence vectors
            #co_diffs[ix] = cosine(mat1[ix],mat2[ix])
            # Since we might have more than two corpora, we generalize by summing up all pairwise distances.
            co_diffs[ix] = sum(pdist([mat[ix] for mat in matrices.values()], 'cosine'))*1./2
        else:
            co_diffs[ix] = -1

    print("%d most different input concepts (based on co-occurrence differences):\n"%num_concepts)
    for ix in np.argsort(co_diffs)[::-1][:num_concepts]:
        print('\n%s (difference %0.2f)'%(in_vocab[ix], co_diffs[ix]))

        for corpus,matrix in matrices.items():
            print("* In '%s' most co-occurring with:"%corpus,
                    '\n  - '.join(['%s (%d)'%(co_vocab[jx],matrix[ix][jx]) for jx in np.argsort(matrix[ix])[::-1][:num_similar]]))


def show_concept_sim_differences(matrices, in_vocab, co_vocab, num_concepts=5, num_similar=10):
    co_sim_matrices = {}
    for corpus,matrix in matrices.items():
        co_sim_matrices[corpus] = squareform(pdist(matrix, 'cosine'))

    print("%d concepts with most different concept similarities\n"%num_concepts)

    for ix in np.argsort(np.sum(np.std(list(co_sim_matrices.values()), axis=0), axis=0))[::-1][:num_concepts]:
        print('Concept "%s"'%in_vocab[ix])

        for corpus,co_sim_matrix in co_sim_matrices.items():
            print("* In '%s' most similar to:"%corpus,
                  ', '.join([in_vocab[jx] for jx in np.argsort(co_sim_matrix[ix])[::-1][1:num_similar+1]]))
        print()


# Word2Vec Based Methods

def show_most_similar_words(models, word):
    print("most similar to '%s':"%word)
    for corpus,model in models.items():
        print("* In '%s':"%corpus,['%s (%0.3f)'%elt for elt in model.wv.most_similar(word)])


def make_w2v_models(corpora, min_count=50, max_vocab_size=2000):
    models = {corpus: gensim.models.Word2Vec([[word.lower() for word in sent] for sent in corpora[corpus]], min_count=min_count)
              for corpus in corpora}

    vocabs = {corpus: model.wv.index2word for corpus,model in models.items()}
    common_vocab = list(set.intersection(*[set(vocab) for vocab in vocabs.values()]))

    # optional: limit the vocabulary size
    common_vocab = common_vocab[:max_vocab_size] # don't want to make computation too expensive

    print("%d words in common vocabulary"%len(common_vocab))

    sim_matrices = {corpus: np.array([model.wv.distances(word, other_words=common_vocab) for word in common_vocab])
                    for corpus,model in models.items()}

    return models, common_vocab, sim_matrices


def w2v_difference_pairs(sim_matrices, vocab, num_display=10):
    """Show word pairs with maximal variation in similarity across corpora."""
    diff_matrix = np.std(np.array([sim_matrix for sim_matrix in sim_matrices.values()]), axis=0)

    print("Word pairs with maximal variation in similarity across corpora\n")
    for x in  np.argsort(np.ravel(diff_matrix))[::-1][::2][:num_display]:
        ix,jx = np.unravel_index(x, diff_matrix.shape)
        print("'{}' and '{}' (std {})".format(vocab[ix], vocab[jx], diff_matrix[ix,jx]))

        for corpus,sim_matrix in sim_matrices.items():
            print("* Similarity in '%s':"%corpus,sim_matrix[ix,jx])
        print()

    return diff_matrix



