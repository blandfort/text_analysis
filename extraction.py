import re
from nltk.sentiment.vader import SentimentIntensityAnalyzer


def filter_document(document, remove_punctuation=False, remove_stopwords=False,
                 pos_blacklist=[], pos_whitelist=None):
    """From a POS-tagged document get a filtered list of tokens."""
    return [token for token in document #TODO add remove_punct and remove_stopwords functionality
            if #(not remove_punctuation or not token.is_punct)
               #and (not remove_stopwords or not token.is_stop)
               #and
            (token[1] not in pos_blacklist)
               and (pos_whitelist is None or token[1] in pos_whitelist)]

def extract_words(document, to_lowercase=False, **kwargs):
    """Get list of words from a POS-tagged document."""
    return [token[0].lower() if to_lowercase else token[0] for token in filter_document(document, **kwargs)]

def extract_hashtags(document, to_lowercase=False, **kwargs):
    """Get a list of hashtags that are present in a given POS-tagged document."""
    return [token[0].lower() if to_lowercase else token[0] for token in filter_document(document, **kwargs)
           if token[0][0]=='#']

#def extract_lemmas(document, **kwargs):
#    return [token.lemma for token in filter_document(document, **kwargs)]

def extract_pos(document, **kwargs):
    return [token[1] for token in filter_document(document, **kwargs)]


# utility function
def ngrams_from_list(list_, n=2):
    """Utility function to extract n-grams from a list.

    Example:
    'n_grams_from_list(range(5), 3) -> [(0,1,2), (1,2,3), (2,3,4)]'"""
    return [el for el in zip(*[list_[i:] for i in range(n)])]


def extract_ngrams(document, n, to_lowercase=False):
    return [' '.join(n_gram) for n_gram in ngrams_from_list([token[0].lower() if to_lowercase else token[0]
                                                             for token in document], n=n)]

def extract_pos_ngrams(document, n):
    return [' '.join(n_gram) for n_gram in ngrams_from_list([token[1] for token in document], n=n)]
    
def extract_sequences(document, pos_tags=['DET','NOUN'], to_lowercase=False):
    seq_len = len(pos_tags)
    n_grams = ngrams_from_list(document, n=seq_len)
    filtered_n_grams = filter(lambda n_gram: all([n_gram[i][1]==pos_tags[i] for i in range(seq_len)]), n_grams)
    return [' '.join([part[0].lower() if to_lowercase else part[0] for part in n_gram])
            for n_gram in filtered_n_grams]


#### Extraction Using Plain Documents (without POS info) ####


def get_sentiment(document, component='compound'):
    analyzer = SentimentIntensityAnalyzer()
    return analyzer.polarity_scores(document)[component]


def get_lower_common_words(document, common_vocab, min_length=3):
    return [word.lower() for word in document if re.match(r'[\w]+', word) and len(word)>=min_length and word.lower() in common_vocab]


sid = SentimentIntensityAnalyzer()

def get_sentiment_words(document, threshold=.2):
    return [word.lower() for word in document if abs(sid.polarity_scores(word)['compound'])>threshold]


def get_sentiment_class(document, thresholds=(-.3,.3)):
    sentiment_score = sid.polarity_scores(' '.join(document))['compound']

    if sentiment_score<=thresholds[0]:
        return 'negative'
    if sentiment_score>=thresholds[-1]:
        return 'positive'
    return 'neutral'


def documents_with_extraction(documents, extraction_function, needle, **kwargs):
    return [' '.join([token[0] for token in doc]) for doc in documents if needle in extraction_function(doc, **kwargs)]


