# FUNCTIONALITY FOR ANALYZING SINGLE CORPORA

import nltk
from nltk.tokenize import sent_tokenize
import string
from extraction import *
from collections import Counter


def simple_stats(documents):
    """Prints out a bunch of statistics about a corpus
    
    @param documents: the corpus, a list of docs(strings)"""
    
    n_docs=len(documents)
    total_words=0
    total_sentences=len(sent_tokenize(' '.join([' '.join(doc) for doc in documents])))
    shortest_doc=float("inf")
    longest_doc=0
    total_characters=0
    unique_words=set()
    
    for doc in documents:
        #TODO write with regex
        include = set(string.digits + string.ascii_uppercase + string.ascii_lowercase + ' ßÜÖÄüöä#')
        tokens = [token for token in doc if all(ch in include for ch in token)]
        words_in_doc=0
        for i in range (len(tokens)):
            token=tokens[i]
            
            words_in_doc+=1
            total_characters+=len(token)
            unique_words.add(token)
           
            
        #longest, shortest doc
        if words_in_doc<shortest_doc:
            shortest_doc=words_in_doc
        
        if words_in_doc>longest_doc:
            longest_doc=words_in_doc
        
        total_words+=words_in_doc
        
    print('Total documents: '+str(n_docs))
    
    #Total words
    print('Total wordcount: '+str(total_words))
    
    #unique words
    print('Total uniqe words: '+str(len(unique_words)))
    
    #Average word length
    if total_words>0:
        wordlength=float(total_characters)/total_words
        print('Average word length: '+str(wordlength) +' characters')
    
    #Average words per doc
    if n_docs>0:
        words_per_doc=float(total_words)/n_docs
        print('Average words per doc: ' + str(words_per_doc))
    
    #Average sentences per doc
    if n_docs>0:
        sentences_per_doc=float(total_sentences)/n_docs
        print('Average sentences per doc: ' +str(sentences_per_doc))
    
    #Average words per sentence
    if total_sentences>0:
        words_per_sentence=float(total_words)/total_sentences
        print('Average words per sentence: ' + str(words_per_sentence))
    
    #longest doc
    print('The biggest document has '+str(longest_doc)+' words.')
    
    #shortest doc
    print('The smallest document has '+str(shortest_doc)+' words.')
    

def k_most_common_items(documents, extraction_fct, k=10, **kwargs):
    item_counts = Counter([item for doc in documents for item in extraction_fct(doc, **kwargs)])
    return item_counts.most_common(k)

def k_most_common_words(documents, k=10, **kwargs):
    return k_most_common_items(documents, extract_words, k=k, **kwargs)

def k_most_common_pos(documents, k=10, **kwargs):
    return k_most_common_items(documents, extract_pos, k=k, **kwargs)

def k_most_common_noun_chunks(documents, k=10, **kwargs):
    return k_most_common_items(documents, extract_noun_chunks, k=k, **kwargs)



