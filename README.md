# Text Analysis Toolbox

Toolbox for analyzing text corpora.


## Set Up

In terminal do:

- cd to project directory

- Create virtual environment

    python -m venv text_venv

- Activate virtual environment

    source text_venv/bin/activate

- Install required packages with pip (see requirements.txt)

    pip install -r requirements.txt

- For running code in jupyter notebooks, install a new kernel using the virtual environment:

    ipython kernel install --user --name=text_venv

- If you would like to run the code with the data used in the examples:

    1. Download the NELA-GT-2018 dataset
    2. Adjust the path settings in `settings.py`


## How To Use

The core of this toolkit is the functionality to compare corpora.


### Input Format

The input is generally be assumed to be given as dict, where corpus names are used as keys and each value is a list of documents (can each be a single sentence or several sentences), each document given as a list of tokens (i.e. documents are assumed to be tokenized).

For example:

    {'corpus_A': [['This', 'is', 'a', 'simple', 'example', '.']],
     'corpus_B': [['Short', 'first', 'sentence'], ['Short', 'second', 'sentence']]}


### Types of Analysis

Currently, there are functions to

- Compare co-occurrences between corpora (e.g. `immigrant` might co-occur a lot with `problem` in corpus A but `diversity` in corpus B)
- Compare concept similarities (e.g. based on the usage in the corpora, `friends` might be used similarly to `family` in corpus C but the most similar word to `friends` in corpus D could be the word `accomplice`)

See the jupyter notebooks for more details and examples.


## More Text Analysis

To mention a few things that can be useful for text analysis but are not implemented in this repo:

- Document clustering
- Topic extraction


